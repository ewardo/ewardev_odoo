# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

{
    'name': "Prestamos",
    'version': '1.0.0',
    'author': "ewardev@hotmail.com",
    'website': "",
    'category': 'payment',
    'license': 'AGPL-3',
    'summary': "Prestamos",
    'description': "Este módulo ayudará a controlar prestamos.",
    'depends': ['base_accounting_kit'],

    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/loans_views.xml',
        
    ],
    'installable': True,
    'application': True,
}
