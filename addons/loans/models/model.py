from odoo import models, fields, api, _
from datetime import date, timedelta
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError as Warning
from odoo.tools.float_utils import float_compare, float_round

class LoanMoney(models.Model):
    _name = 'loan.money'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Prestamo de dinero'
    _order = 'date asc'
    
    def _compute_display_name(self):
        for record in self:
            record.display_name = 'Prestamo de '+str(record.partner_id.name) +' en la fecha ' + str(record.date)
            record.name = record.display_name

    display_name = fields.Char(string="Nombre a mostrar", 
        compute=_compute_display_name)

    name = fields.Char(string="Nombre", default='*')

    state = fields.Selection(string="Estado",
        selection=[ ('draft', 'Borrador'),                                        
                    ('validated', 'Validado'),
                    ('cancel', 'Cancelado')], default="draft")

    company_id = fields.Many2one('res.company', string='Compañia', 
        store=True, readonly=True, 
        default=lambda self: self.env.user.company_id )

    currency_id = fields.Many2one('res.currency', 
        related='company_id.currency_id',
        string='Moneda', store=True, readonly=True)

    date = fields.Date(string='Fecha de prestamo', 
        default=date.today(), track_visibility='onchange', 
        readonly=True, states={'draft': [('readonly', False)]})

    partner_id = fields.Many2one('res.partner', 
        string='Prestador', track_visibility='onchange', 
        readonly=True, states={'draft': [('readonly', False)]})
    
    fees = fields.Integer(string='Cuotas', default=1, 
        readonly=True, states={'draft': [('readonly', False)]})
    
    amount_total = fields.Monetary(string='Total', 
        store=True, compute='_compute_amount')
    
    borrowed_amount = fields.Monetary(string='Cantidad prestada', 
        readonly=True, states={'draft': [('readonly', False)]})

    line_ids = fields.One2many('loan.money.lines', 
        inverse_name = 'loan_money_id', ondelete='cascade', 
        readonly=True, states={'draft': [('readonly', False)]})
    
    notes = fields.Text()

    @api.depends('line_ids')
    def _compute_amount(self):
        for record in self:
            record.amount_total = sum([x.amount for x in record.line_ids])

    @api.constrains('line_ids.amount')
    def constrains_lines(self):
        for record in self:
            if [x.amount for x in record.line_ids if x.amount <= 0]:
                raise Warning('Error, esta dejando una linea con valor 0')
            print(record.amount_total ,'!=', record.borrowed_amount)
            if record.borrowed_amount > 0 and (record.amount_total != record.borrowed_amount):
                raise Warning('Error, el valor total de las lineas no coincide con la Cantidad prestada.')

    @api.onchange('borrowed_amount','fees')
    def generate_lines(self):
        ###se agrega control en caso de necesite agregar o quitar ctvs
        for record in self:
            pay=0
            control = 0
            list_vals=[]
            record.line_ids = [(6, 0, [])]
            if record.borrowed_amount > 0 and record.fees > 0:
                pay=float_round(record.borrowed_amount/record.fees,precision_rounding=self.currency_id.rounding)                                
                if float_compare((pay*record.fees), record.borrowed_amount,precision_rounding=self.currency_id.rounding):
                    control = record.borrowed_amount-(pay*record.fees)  
            for fees in range(0, record.fees):
                fees += 1
                payment_date = record.date + relativedelta(months=+fees) 
                vals = { 'name':fees,
                        'amount':pay + control if fees == record.fees else pay,
                        'payment_date':payment_date}
                list_vals.append((0, 0, vals))
            record.line_ids = list_vals   

        return

    def action_validated(self):
        for record in self:
            record.write({'state':'validated'})

class LoanMoneyLines(models.Model):
    _name = 'loan.money.lines'
    _description = 'Lineas de prestamo de dinero'

    name = fields.Char('Cuota')
    
    loan_money_id = fields.Many2one('loan.money')
    
    payment_date = fields.Date(string='Fecha de prestamo')
    
    currency_id = fields.Many2one('res.currency', 
        related='loan_money_id.currency_id')
    
    amount = fields.Monetary(string='Monto')


