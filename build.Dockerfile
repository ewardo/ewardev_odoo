FROM debian:stretch
MAINTAINER Kleber Arias <ewardev@hotmail.com>
# Generate locale C.UTF-8 for postgres and general locale data
ENV LANG C.UTF-8

COPY ./deb/odoo_13.0.latest_all.deb /root/odoo.deb
# Install some deps, lessc and less-plugin-clean-css, and wkhtmltopdf
RUN set -x; \
        apt-get update \
        && apt-get install -y --no-install-recommends \
            ca-certificates \
            curl \
            libssl-dev \
            node-less \
            python3-pip \
            python3-pyldap \
            python3-qrcode \
            python3-renderpm \
            python3-setuptools \
            python3-vobject \
            python3-watchdog \
            python3-dev \
            xz-utils \
            default-jdk \
            build-essential \
        && curl -o wkhtmltox.deb -SL https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.stretch_amd64.deb \
        && echo '7e35a63f9db14f93ec7feeb0fce76b30c08f2057 wkhtmltox.deb' | sha1sum -c - \
        && dpkg --force-depends -i wkhtmltox.deb\
        && apt-get -y install -f --no-install-recommends \
        && rm -rf /var/lib/apt/lists/* wkhtmltox.deb \
        && dpkg --force-depends -i /root/odoo.deb \
        && apt-get update \
        && apt-get -y install -f --no-install-recommends \
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/* /root/odoo.deb

# Copy entrypoint script and Odoo configuration file
RUN pip3 install num2words xlwt python-stdnum py3o.template py3o.formats cachetools cerberus pyquerystring parse_accept_language jsondiff firebase-admin
COPY ./entrypoint.sh /
COPY ./odoo.conf /etc/odoo/odoo.conf
RUN chown odoo /etc/odoo/odoo.conf

# Mount /var/lib/odoo to allow restoring filestore and /mnt/extra-addons for users addons
RUN mkdir -p /mnt/extra-addons \
        && chown -R odoo /mnt/extra-addons
VOLUME ["/var/lib/odoo", "/mnt/extra-addons"]

# Expose Odoo services
EXPOSE 8069 8071

# Set the default config file
ENV ODOO_RC /etc/odoo/odoo.conf

COPY ./addons /mnt/extra-addons

# Set default user when running the container
USER odoo

ENTRYPOINT ["/entrypoint.sh"]
CMD ["odoo"]